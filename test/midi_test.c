#include <xc.h>
#include <stdint.h>

#include "../source/system/config_bits.h"
#include "../source/system/system.h"
#include "../source/drivers/timer.h"
#include "../source/drivers/uart.h"
#include "../source/drivers/midi.h"
#include "../source/drivers/softpot.h"

void good_midi_drivers();

void play()
{
    static int note;
    midi_noteon(MIDI_CHAN00, note, 0);
    midi_noteon(MIDI_CHAN00, ++note, 127);
    
//    static int toggle;
//    midi_noteon(MIDI_CHAN05, 60, (toggle) ? 127 : 0);
//    toggle = !toggle;
    
//    static unsigned char pitch = 0;
//    midi_message_transmit(0b11100000, 0, pitch++ % 128);
}

int16_t main(void) {
    
    int i;
    unsigned char bend = MIDI_P_BEND_CENTER;
    unsigned char note = 60;
    unsigned char old_note;
    midi_note mnote;
    io_init();
    timer_init();
    UART_Init();
    DELAY_MS(500);
//    timer_register_handler(play, 50);
    
    midi_init_controller();
    midi_message_transmit(MIDI_CONTROL_CHANGE & MIDI_CHAN00, MIDI_PORTAMENTO_ON_CMD, MIDI_PORTAMENTO_ON);
    midi_message_transmit(MIDI_CONTROL_CHANGE & MIDI_CHAN00, MIDI_PORTAMENTO_TIME,0);
    midi_message_transmit(MIDI_P_BEND_CMD & MIDI_CHAN00, 0, bend);
    
//    DELAY_MS(2000);
    /*midi_noteon(MIDI_CHAN00, note, 127);
    while(note < 65)
    {
        midi_message_transmit(MIDI_P_BEND_CMD & MIDI_CHAN00, 0, bend);
        while(bend <= MIDI_P_BEND_CENTER + MIDI_P_BEND_HALF)
        {
            midi_message_transmit(MIDI_P_BEND_CMD & MIDI_CHAN00, 0, bend++);
            DELAY_MS(50);
        }
        bend = MIDI_P_BEND_CENTER;
        
        midi_message_transmit(MIDI_CONTROL_CHANGE & MIDI_CHAN00, MIDI_PORTAMENTO_CONTROL, note);
        midi_noteon(MIDI_CHAN00, note + 1, 127);
        midi_noteoff(MIDI_CHAN00, note, 127);
        
        note++;
    }
    midi_noteoff(MIDI_CHAN00, note, 127);*/
    
    /*mnote.note = note;
    mnote.bend = bend;
    mnote.velocity = 127;
    old_note = midi_start_slide(MIDI_CHAN00, mnote);
    while(note < 70)
    {
        mnote.note = note;
        bend = MIDI_P_BEND_CENTER;
        while(bend < MIDI_P_BEND_CENTER + MIDI_P_BEND_HALF)
        {
            mnote.bend = bend;
            old_note = midi_change_slide(MIDI_CHAN00, old_note, mnote);
            DELAY_MS(50);
            bend++;
        }
        note++;
    }
    midi_noteoff(MIDI_CHAN00, old_note, 127);
    
    DELAY_MS(500);*/
//
    //portamento_testing();
    good_midi_drivers();
    
    
    while(1);
    
    
    return 0;
}


void portamento_testing()
{
    midi_message_transmit(MIDI_CONTROL_CHANGE, MIDI_PORTAMENTO_ON_CMD, MIDI_PORTAMENTO_ON);
    midi_message_transmit(MIDI_CONTROL_CHANGE, MIDI_PORTAMENTO_TIME, 20);
    
    DELAY_MS(200);
    midi_noteon(MIDI_CHAN00, 0x3C, 0x40);
    DELAY_MS(200);
    midi_message_transmit(MIDI_CONTROL_CHANGE, MIDI_PORTAMENTO_CONTROL, 0x3C);
    DELAY_MS(1000);
    midi_noteon(MIDI_CHAN00, 0x40, 0x40);
    midi_noteoff(MIDI_CHAN00, 0x3C, 0x40);
    DELAY_MS(3000);
    midi_noteoff(MIDI_CHAN00, 0x40, 0x40);
    DELAY_MS(1000);
}

void good_midi_drivers()
{
    midi_partial p0, p1, p2, p3, p4, p5, p6, p7;
    midi_note mnote;
    midi_partial p;
    char pnum = 0;
    char old_note;
    
    midi_init_partial(&p0,0);
    midi_init_partial(&p1,1);
    midi_init_partial(&p2,2);
    midi_init_partial(&p3,3);
    midi_init_partial(&p4,4);
    midi_init_partial(&p5,5);
    midi_init_partial(&p6,6);
    midi_init_partial(&p7,7);
    
    unsigned int position = SFPT_MIN;
    
    for(pnum = 0; pnum < 8; pnum ++)
    {
        if(pnum == 0)
            p = p0;
        else if(pnum == 1)
            p = p1;
        else if(pnum == 2)
            p = p2;
        else if(pnum == 3)
            p = p3;
        else if(pnum == 4)
            p = p4;
        else if(pnum == 5)
            p = p5;
        else if(pnum == 6)
            p = p6;
        else if(pnum == 7)
            p = p7;
                   
        position = SFPT_MIN;
        mnote = softpot_get_note(position, &p);
        midi_start_slide(MIDI_CHAN00, mnote);
        DELAY_MS(1000);
        for(position = SFPT_MIN; position < SFPT_MAX; position ++)
        {
            old_note = mnote.note;
            mnote = softpot_get_note(position, &p);
            midi_change_slide(MIDI_CHAN00, old_note, mnote);
            DELAY_MS(10);
        }
        midi_noteoff(MIDI_CHAN00, mnote.note, 127);
    }
    
    
    
    
    
    
    
    
}


