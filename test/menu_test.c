/*
 * File:   menu_test.c
 * Author: ramachas
 *
 * Created on April 21, 2016, 6:26 PM
 */


#include <xc.h>
#include <stdlib.h>
#include <stdio.h>

#include "../source/system/config_bits.h"
#include "../source/system/system.h"

#include "../source/drivers/spi.h"
#include "../source/drivers/timer.h"

#include "../source/drivers/lcd.h"
#include "../source/app/menu.h"
#include "../source/app/button.h"

void disp_option(int option)
{
    char buf[17];
    sprintf(buf, "Option: %d", option);
    
    lcd_send_i(LCD_CLR);
    lcd_chgline(LINE1);
    lcd_pmsg(buf);
    
    while(1)
    {
        if(get_button_falling_edge(BUTTON_LEFT))
            break;
    }
}

int main(void) {
    /*
     * Initialization
     */
    io_init();
    timer_init();
    spi_init();
    lcd_init();
    
    timer_register_handler(sample_buttons, 2);
    
    
    /*
     * Generate Menus
     */
    
    menu_setting_t ch1_setting = menu_create_setting("Primary Channel", disp_option);
    menu_setting_add_option(&ch1_setting, "1");
    menu_setting_add_option(&ch1_setting, "2");
    menu_setting_add_option(&ch1_setting, "3");
    menu_setting_add_option(&ch1_setting, "4");
    menu_setting_add_option(&ch1_setting, "5");
    menu_setting_add_option(&ch1_setting, "6");
    menu_setting_add_option(&ch1_setting, "7");
    menu_setting_add_option(&ch1_setting, "8");
    menu_setting_add_option(&ch1_setting, "9");
    menu_setting_add_option(&ch1_setting, "10");
    menu_setting_add_option(&ch1_setting, "11");
    menu_setting_add_option(&ch1_setting, "12");
    menu_setting_add_option(&ch1_setting, "13");
    menu_setting_add_option(&ch1_setting, "14");
    menu_setting_add_option(&ch1_setting, "15");
    menu_setting_add_option(&ch1_setting, "16");
    
    menu_setting_t ch2_setting = menu_create_setting("Secondary Ch.", disp_option);
    menu_setting_add_option(&ch2_setting, "1");
    menu_setting_add_option(&ch2_setting, "2");
    menu_setting_add_option(&ch2_setting, "3");
    menu_setting_add_option(&ch2_setting, "4");
    menu_setting_add_option(&ch2_setting, "5");
    menu_setting_add_option(&ch2_setting, "6");
    menu_setting_add_option(&ch2_setting, "7");
    menu_setting_add_option(&ch2_setting, "8");
    menu_setting_add_option(&ch2_setting, "9");
    menu_setting_add_option(&ch2_setting, "10");
    menu_setting_add_option(&ch2_setting, "11");
    menu_setting_add_option(&ch2_setting, "12");
    menu_setting_add_option(&ch2_setting, "13");
    menu_setting_add_option(&ch2_setting, "14");
    menu_setting_add_option(&ch2_setting, "15");
    menu_setting_add_option(&ch2_setting, "16");
    
    menu_t channel_menu = menu_create();
    menu_add_child_setting(&channel_menu, "Primary Ch.", &ch1_setting);
    menu_add_child_setting(&channel_menu, "Secondary Ch.", &ch2_setting);
    

    menu_setting_t instr_setting = menu_create_setting("Instrument", disp_option);
    menu_setting_add_option(&instr_setting, "Trombone");
    menu_setting_add_option(&instr_setting, "Guitar");
    menu_setting_add_option(&instr_setting, "Piano");
    

    menu_setting_t tilt_setting = menu_create_setting("Tilt Effect", disp_option);
    menu_setting_add_option(&tilt_setting, "Channel Phase");
    menu_setting_add_option(&tilt_setting, "Instr. Change");
    menu_setting_add_option(&tilt_setting, "Volume Control");

    menu_setting_t key_setting = menu_create_setting("D Key Effect", disp_option);
    menu_setting_add_option(&key_setting, "Power Chord");
    
    menu_t effects_menu = menu_create();
    menu_add_child_setting(&effects_menu, "Tilt Effect", &tilt_setting);
    menu_add_child_setting(&effects_menu, "Key Effect", &key_setting);
    
    menu_t config_menu = menu_create();
    menu_add_child_menu(&config_menu, "MIDI Channel", &channel_menu);
    menu_add_child_menu(&config_menu, "Effects", &effects_menu);
    menu_add_child_setting(&config_menu, "Instrument", &instr_setting);
    
    menu_t main_menu = menu_create();
    menu_add_child_menu(&main_menu, "Configuration", &config_menu);
    
    /*
     * Use Menu
     */
    menu_set_main(&main_menu);
    menu_update_display();
    
    
    while(1)
    {
        if(get_button_falling_edge(BUTTON_DOWN))
        {
            menu_next();
        }
        else if(get_button_falling_edge(BUTTON_LEFT))
        {
            menu_cancel();
        }
        else if(get_button_falling_edge(BUTTON_RIGHT))
        {
            menu_enter();
        }
    }
    
//    return 0;
}
