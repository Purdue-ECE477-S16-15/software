#include <xc.h>
#include <stdint.h>

#include "../source/system/config_bits.h"
#include "../source/system/system.h"
#include "../source/drivers/timer.h"

void test_func()
{
    static int blah;
    blah = !blah;
    PORTBbits.RB1 = blah;
}

int16_t main(void) {
    timer_init();
    
    TRISBbits.TRISB1 = 0;
    
    timer_register_handler(test_func, 1);
    
    while(1);
    
    return 0;
}
