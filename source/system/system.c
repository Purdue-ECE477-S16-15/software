#include <xc.h>

#include "system.h"

void io_init()
{
#if defined(__PIC24FJ128GA204__)
    /*
     * SPI
     */
    ANSBbits.ANSB1 = 0;     // Port B1 Digital
    
    TRISBbits.TRISB1 = 0;   // Port B1 Output
    TRISBbits.TRISB8 = 0;   // Port B8 Output
    
    RPOR0bits.RP1R = _RPOUT_SCK1OUT;
    RPOR4bits.RP8R = _RPOUT_SDO1;
    
    /*
     * I2C
     */
    TRISBbits.TRISB2 = 0;
    TRISBbits.TRISB3 = 0;
    
    ANSBbits.ANSB2 = 0;
    ANSBbits.ANSB3 = 0;
    
    /*
     * UART
     */
    TRISBbits.TRISB7 = 0;
    
    RPOR3bits.RP7R = _RPOUT_U2TX;
    
    /*
     * LCD
     */
    TRISBbits.TRISB10 = 0;
    TRISBbits.TRISB11 = 0;
    TRISBbits.TRISB12 = 0;
    
    ANSBbits.ANSB12 = 0;
    
    /*
     * GPIO
     */
    // Switches are on all bits of Port C
    ANSC = 0;   // Port C Digital
    TRISC = 0xFF;  // Port C Input
#endif
}