/* 
 * File:   system.h
 * Author: ramachas
 *
 * Created on February 24, 2016, 11:10 AM
 */

#ifndef SYSTEM_H
#define	SYSTEM_H

#ifdef	__cplusplus
extern "C" {
#endif

// TODO Clean this stuff up in general, maybe rename this file

///******************************************************************************/
///* System Level #define Macros                                                */
///******************************************************************************/
//
///* TODO Define system operating frequency */
//
///* Microcontroller MIPs (FCY) */
#define SYS_FREQ        8000000L
#define FCY             SYS_FREQ/2
    
#define CYCLES_PER_MS ((unsigned long)(FCY * 0.001))        //instruction cycles per millisecond
#define CYCLES_PER_US ((unsigned long)(FCY * 0.000001))   //instruction cycles per microsecond
#define DELAY_MS(ms)  __delay32(CYCLES_PER_MS * ((unsigned long) ms));   //__delay32 is provided by the compiler, delay some # of milliseconds

#define DELAY_US(us)  __delay32(CYCLES_PER_US * ((unsigned long) us));    //delay some number of microseconds
  
 

//
///******************************************************************************/
///* System Function Prototypes                                                 */
///******************************************************************************/
//
///* Custom oscillator configuration funtions, reset source evaluation
//functions, and other non-peripheral microcontroller initialization functions
//go here. */
//
//void ConfigureOscillator(void); /* Handles clock switching/osc initialization */

/**
 * Initialize Peripheral Pin Select for I/O Pins
 */
void io_init(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SYSTEM_H */

