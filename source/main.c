#include <xc.h>
#include <stdint.h>
#include <libpic30.h>

#include "system/config_bits.h"
#include "system/system.h"
#include "drivers/timer.h"
#include "drivers/spi.h"
#include "drivers/imu.h"
#include "drivers/i2c.h"
#include "drivers/uart.h"
#include "drivers/lcd.h"
#include "drivers/midi.h"
#include "drivers/adc.h"
#include "drivers/softpot.h"
#include "app/button.h"
#include "app/app.h"

int16_t main(void) {
    
    
    io_init();
    spi_init();
    timer_init();
    I2C_init();
    IMU_init();
    UART_Init();
    ADC_SetConfiguration();
    ADC_ChannelEnable();
    
    InitApp();
    
    DELAY_MS(300);
    midi_init_controller(MIDI_CHAN00);
    lcd_init();
    
    lcd_pmsg("Welcome to the");
    lcd_chgline(LINE2);
    lcd_pmsg("T.R.O.M.B.O.N.E.");
    lcd_chgline(LINE1);
    
    DELAY_MS(3000);
    menu_init();
    
    timer_register_handler(sample_buttons, 2);
    timer_register_handler(update_midi_state, 2);

    
    while(1){
        //IMU_progress();
        menu_progress();
    }
}
