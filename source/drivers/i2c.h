/* 
 * File:   I2C.h
 * Author: James Alliger <jalliger@purdue.edu>
 *
 * Created on February 20, 2016, 2:40 PM
 */

#ifndef I2C_H
#define	I2C_H
#include <stdint.h>

#define MAX_RETRY   3

#define I2C_IDLE    0
#define I2C_WRITE   1
#define I2C_READ    2
#define I2C_ERR     0xFFFF

// EEPROM DATA OBJECT
typedef struct
{
    uint16_t    *buff;
    uint16_t    n;
    uint16_t    addr;
    uint16_t    csel;
} I2CEMEM_DATA;
    
typedef struct
    {
        uint16_t        cmd;
        I2CEMEM_DATA    *oData;
    }
    I2CEMEM_DRV;
    
void I2C_init();
inline void I2C_start();

#ifdef __PIC24FJ128GA010__

#define I2CENABLE I2C2CONbits.I2CEN
#define I2CSTART  I2C2CONbits.SEN
#define I2CPEN    I2C2CONbits.PEN
#define I2CRSEN   I2C2CONbits.RSEN
#define I2CRCEN   I2C2CONbits.RCEN
#define I2CACKDT  I2C2CONbits.ACKDT
#define I2CACKEN  I2C2CONbits.ACKEN
#elif __PIC24FJ128GA204__

#define I2CENABLE I2C2CONLbits.I2CEN
#define I2CSTART  I2C2CONLbits.SEN
#define I2CPEN    I2C2CONLbits.PEN
#define I2CRSEN   I2C2CONLbits.RSEN
#define I2CRCEN   I2C2CONLbits.RCEN
#define I2CACKDT  I2C2CONLbits.ACKDT
#define I2CACKEN  I2C2CONLbits.ACKEN

#endif
#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* I2C_H */
