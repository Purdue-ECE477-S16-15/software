/* 
 * SPI Transmit-Only Driver
 */

#ifndef SPI_H
#define	SPI_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>

/**
 * Initialize the SPI1 peripheral.
 */    
void spi_init(void);

/**
 * Transmit one byte of data over on the SPI1 peripheral
 * 
 * @param data Byte of data to send.
 */
void spi_write(uint8_t data);


#ifdef	__cplusplus
}
#endif

#endif	/* SPI_H */

