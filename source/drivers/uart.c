/* UART CONTROL logic
 * contains code to use buffers and interrupts to transmit uart data
 * Heavily sampled from RN4020 Bluetooth LE PICtail application on an Explorer-16 board
 * code From microchip
 * 
 */
#include <xc.h>
#include "uart.h"
static volatile unsigned char txBuffer[SIZE_TxBuffer], *txBufRdPtr, *txBufWrPtr;


void UART_Init(void)
{
    txBufRdPtr = txBufWrPtr = &txBuffer[0]; //initialize buffer
    
    U2BRG = 7; //set baud rate
    
    U2MODEbits.UARTEN = 1;// enable uart
    U2STAbits.UTXEN = 1;//enable transmit mode
    //from default, interrupt generated on single byte transmit
}

void UART_TxStart(void)
{
    IEC1bits.U2TXIE = 1; //enable transmit interrupts
}

//**********************************************************************************************************************
// Write a byte to the transmit buffer

void WriteTxBuffer(unsigned char TxByte)
{
    *txBufWrPtr++ = TxByte;                 //Put the byte in the transmit buffer and increment the pointer
    if (txBufWrPtr > &txBuffer[SIZE_RxBuffer - 1]) //Check if at end of buffer
        txBufWrPtr = &txBuffer[0];          //Wrap pointer to beginning
}

//**********************************************************************************************************************
// Interrupt routine for UART transmit interrupts

void __attribute__((interrupt,no_auto_psv)) _U2TXInterrupt(void)
{
    if (txBufRdPtr != txBufWrPtr)           //Check if more data is in the buffer
    {
        IFS1bits.U2TXIF = 0;                //Clear UART 1 Receive interrupt flag
        U2TXREG = *txBufRdPtr++;            //Get next byte from the buffer
        if (txBufRdPtr > &txBuffer[SIZE_TxBuffer - 1])  //Check if end of buffer
            txBufRdPtr = &txBuffer[0];      //Wrap pointer to beginning
    }
    else
        IEC1bits.U2TXIE = 0;                //No data so stop interrupts
}
