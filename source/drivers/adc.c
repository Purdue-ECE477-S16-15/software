#include <xc.h>
#include "adc.h"

bool ADC_SetConfiguration()
{   
    AD1CON1bits.SSRC = 0b0111;
    AD1CON1bits.MODE12 = 0;
    
    AD1CON2bits.PVCFG = 0b00;
    AD1CON2bits.NVCFG0 = 0;
    AD1CON2bits.CSCNA = 1;
    AD1CON2bits.SMPI = 2;
    AD1CON2bits.BUFREGEN = 1;
    
    AD1CON3bits.ADRC = 0;
    AD1CON3bits.ADCS = 0;
    AD1CON3bits.SAMC = 15;
    
    ANSAbits.ANSA1 = 1;
    ANSBbits.ANSB15 = 1;
    AD1CHSbits.CH0NA = 0b000;
    AD1CSSLbits.CSS9 = 1;
    AD1CSSLbits.CSS1 = 1;
    
    //IEC0bits.AD1IE = 1;

    AD1CON1bits.ADON = 1;
    AD1CON1bits.ASAM = 1;

    //AD1CON1bits.SAMP = 1;
    return true;
}

bool ADC_ChannelEnable()
{
    return true;
}

void __attribute__ ((__interupt, auto_psv))_ADC1Interrupt(void)
{
   unsigned int trial = PRESSURESENS_BUF;
   trial += trial;
   IFS0bits.AD1IF = 0;
   
}