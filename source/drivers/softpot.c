/*
 * File:   softpot.c
 * Author: lneuman
 *
 * Created on April 11, 2016, 11:47 AM
 */


#include "xc.h"
#include "softpot.h"

midi_note softpot_get_note(unsigned int position, midi_partial * partial)
{
    unsigned int p_under;
    unsigned int p_over;
    char dp_under;
    char dp_over;
    midi_note mnote;
    mnote.velocity = 127;//TODO: change this
    softpot_resolve_position(position, &p_under, &p_over, &dp_under, &dp_over);
    
    if((position - p_under) < (p_over - position))
    {//use p_under
        mnote.note = partial->positions[dp_under - 1];
        mnote.bend = MIDI_P_BEND_CENTER - 
                ((position - p_under) * MIDI_P_BEND_HALF / SFPT_HALF_STEP);
    }
    else
    {//user p_over
        mnote.note = partial->positions[dp_over - 1];
        mnote.bend = MIDI_P_BEND_CENTER +
                ((p_over - position) * MIDI_P_BEND_HALF / SFPT_HALF_STEP);
    }
    return mnote;
}

void softpot_resolve_position(unsigned int position, unsigned int * under, unsigned int * over, char * d_under, char * d_over)
{
    *under = SFPT_1;
    *over = SFPT_2;
    *d_under = 1;
    *d_over = 2;
    
    if(position >= SFPT_2)
    {
        *under = SFPT_2;
        *over = SFPT_3;
        *d_under = 2;
        *d_over = 3;
    }
    if(position >= SFPT_3)
    {
        *under = SFPT_3;
        *over = SFPT_4;
        *d_under = 3;
        *d_over = 4;
    }
    if(position >= SFPT_4)
    {
        *under = SFPT_4;
        *over = SFPT_5;
        *d_under = 4;
        *d_over = 5;
    }
    if(position >= SFPT_5)
    {
        *under = SFPT_5;
        *over = SFPT_6;
        *d_under = 5;
        *d_over = 6;
    }
    if(position >= SFPT_6)
    {
        *under = SFPT_6;
        *over = SFPT_7;
        *d_under = 6;
        *d_over = 7;
    }
    if(position >= SFPT_7)
    {
        *under = SFPT_7;
        *over = SFPT_MAX;
        *d_under = 7;
        *d_over = 8;
    }
}
