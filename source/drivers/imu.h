/* 
 * LSM303 Accelerometer/Compass Driver
 */

#ifndef IMU_H
#define	IMU_H

#include <stdint.h>


#ifdef	__cplusplus
extern "C" {
#endif
#define MAX_GRAV_Y 17260

#define LSM303_MAG 0x1E
#define LSM303_ACC 0x1D
//magnometer registers  
#define MAG_WHO_AM_I   0x0F
#define MAG_CTRL_REG1  0x20
#define MAG_CTRL_REG2  0x21
#define MAG_CTRL_REG3  0x22
#define MAG_CTRL_REG4  0x23
#define MAG_CTRL_REG5  0x24
#define MAG_STATUS_REG 0x27
#define MAG_OUTX_L     0x28
#define MAG_OUTX_H     0x29
#define MAG_OUTY_L     0x2A
#define MAG_OUTY_H     0x2B
#define MAG_OUTZ_L     0x2C
#define MAG_OUTZ_H     0x2D
#define MAG_TEMP_OUT_L 0x2E
#define MAG_TEMP_OUT_H 0x2F
#define MAG_INT_CFG    0x30
#define MAG_INT_SRC    0x31
#define MAG_INT_THS_L  0x32
#define MAG_INT_THS_H  0x33

    //accelerometer registers
#define ACC_TEMP_L        0x0B
#define ACC_TEMP_H        0x0C
#define ACC_ACT_TSH       0x1E
#define ACC_ACT_DUR       0x1F
#define ACC_WHO_AM_I      0x0F
#define ACC_CTRL1         0x20
#define ACC_CTRL2         0x21
#define ACC_CTRL3         0x22
#define ACC_CTRL4         0x23
#define ACC_CTRL5         0x24
#define ACC_CTRL6         0x25
#define ACC_CTRL7         0x26
#define ACC_STATUS        0x27
#define ACC_OUT_X_L       0x28
#define ACC_OUT_X_H       0x29
#define ACC_OUT_Y_L       0x2A
#define ACC_OUT_Y_H       0x2B
#define ACC_OUT_Z_L       0x2C
#define ACC_OUT_Z_H       0x2D
#define ACC_FIFO_CTRL     0x2E
#define ACC_FIFO_SRC      0x2F
#define ACC_IG_CFG1       0x30
#define ACC_IG_SRC1       0x31
#define ACC_IG_THS_X1     0x32
#define ACC_IG_THS_Y1     0x33
#define ACC_IG_THS_Z1     0x34
#define ACC_IG_DUR1       0x35
#define ACC_IG_CFG2       0x36
#define ACC_IG_SRC2       0x37
#define ACC_IG_THS2       0x38
#define ACC_IG_DUR2       0x39
#define ACC_XL_REFERENCE  0x3A
#define ACC_XH_REFERENCE  0x3B
#define ACC_YL_REFERENCE  0x3C
#define ACC_YH_REFERENCE  0x3D
#define ACC_ZL_REFERENCE  0x3E
#define ACC_ZH_REFERENCE  0x3F
    
#define IMU_INC_MASK 0x80
    
    typedef enum 
    {
        idle,
        start_acc_read,
        start_mag_read,
        read_mag,
        read_acc,
        calc_pitch,
        calc_heading
                
    }IMU_state;
    
    typedef struct
    {
        int16_t x;
        int16_t y;
        int16_t z;
    }Cartesian_t;
/**
 * Initialize the accelerometer driver.
 * 
 * Also initializes the I2C driver.
 */
void IMU_init();

/**
 * Allow the accelerometer driver state machine to progress.
 * 
 * This function needs to be called regularly from the main program loop to
 * share compute time with the driver.
 */
void IMU_progress();

/**
 * Return the last angle computed and buffered by the accelerometer driver.
 * 
 * @return the angle relative to the ground, in radians
 */
int16_t acc_get_angle();




#ifdef	__cplusplus
}
#endif

#endif	/* IMU_H */
