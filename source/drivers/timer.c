/* 
 * Timer Peripheral Driver Source
 */

#include <stdlib.h>
//#include <p24FJ128GA010.h>
#include <xc.h>
#include <stddef.h>
#include "timer.h"

/*********** Declarations ***********/

// Array of function pointers to event handlers.
//static void(* handlers[TIMER_NUM_HANDLERS])();

static timer_handler_t handlers[TIMER_NUM_HANDLERS];

/*********** Function Definitions ***********/

/**
 * Initialize the Timer1 Peripheral to interrupt at 1ms.
 */
static void _timer1_init()
{
    T1CONbits.TCKPS = 0;    // Prescaler of 1
    PR1 = 4000;             // Period 4000          (4 Mhz / 1 ms = 4000)
    IEC0bits.T1IE = 1;      // Enable Interrupts
    
    T1CONbits.TON = 1;      // Start Timer
    
}

void timer_init()
{
    // Set all event handlers to NULL.
    int i;
    for (i = 0; i < TIMER_NUM_HANDLERS; i++)
    {
        handlers[i].func = NULL;
        handlers[i].time = 0;
        handlers[i].count = 0;
    }
    
    // Initialize the peripheral.
    _timer1_init();
}

void timer_register_handler(void(*func)(), int time)
{
    int i;
    for (i = 0; i < TIMER_NUM_HANDLERS; i++)
    {
        if(handlers[i].func == NULL)
        {
            handlers[i].func = func;
            handlers[i].time = time;
            handlers[i].count = 0;
            
            return; // Success
        }
    }
    
    return; // Fail
}


void __attribute__ ((__interrupt__, auto_psv)) _T1Interrupt(void)
{
    int i;
    for (i = 0; i < TIMER_NUM_HANDLERS && handlers[i].func != NULL; i++)
    {
        if(handlers[i].count++ % handlers[i].time == 0)
            handlers[i].func();
    }

    
    IFS0bits.T1IF = 0;
}