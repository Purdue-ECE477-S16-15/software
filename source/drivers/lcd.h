/* 
 * 2-Line Hitachi-HD44780-Based LCD Driver
 */

#ifndef LCD_H
#define	LCD_H

#include <xc.h> // include processor files - each processor file is guarded.  
#include <stdint.h>

#define LCD_RS PORTBbits.RB12	// RS pin mask (RB12)
#define LCD_CLK PORTBbits.RB10	// EN/CLK pin mask (RB10)
#define LCD_RW PORTBbits.RB11   // R/W' pin (RB11)
#define LCD_ON 0x0C	// LCD initialization command
#define LCD_CLR 0x01	// LCD clear display command
#define LCD_TWOLINE 0x38	// LCD 2-line enable command
#define LCD_CURMOV 0xFE	// LCD cursor move instruction
#define LCD_BACK 0x10       // LCD cursor move one to left
    
#define LINE1 0x80     //;LCD line 1 cursor position
#define LINE2 0xC0     //;LCD line 2 cursor position

void lcd_shiftout(char x);
void lcd_send_byte(uint8_t  x);
void lcd_send_i(char x);
void lcd_chgline(char x);
void lcd_print_c(char x);
void lcd_pmsg(char x[]);

void lcd_init(void);

#endif	/* LCD_H */

