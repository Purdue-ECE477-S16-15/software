/* 
 * Timer Peripheral Driver
 * 
 * Allows registering functions as event handlers to occur on 1 ms intervals.
 */

#ifndef TIMER_H
#define	TIMER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdint.h>
    
#define TIMER_NUM_HANDLERS 10
    
typedef struct {
    void (* func)();
    uint16_t time;
    uint16_t count;
} timer_handler_t;

/**
 * Initialize the timer peripheral driver
 */
void timer_init();

/**
 * Register a function as an event handler to fire on an interval.
 * @param func Function to trigger.
 * @param time Time between events in milliseconds.
 */
void timer_register_handler(void (* func)(), int time);


#ifdef	__cplusplus
}
#endif

#endif	/* TIMER_H */

