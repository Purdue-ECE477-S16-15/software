/* 
 * File:   uart.h
 * Author: jalliger
 *
 * Created on February 17, 2016, 10:41 AM
 */

#ifndef UART_H
#define	UART_H

#define UART_BUFFER
//Buffer sizes
#define SIZE_RxBuffer 1024//128
#define SIZE_TxBuffer 1024//128
void UART_Init(void);
void WriteTxBuffer(unsigned char TxByte);
void UART_TxStart(void);
#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* UART_H */

