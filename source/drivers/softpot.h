
#ifndef SOFTPOT_H
#define	SOFTPOT_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include "midi.h"

#define SFPT_MAX 1013
#define SFPT_MIN 67
#define SFPT_EXTRA 0

#define SFPT_7 SFPT_MAX - SFPT_EXTRA
#define SFPT_1 SFPT_MIN
#define SFPT_6_STEPS (SFPT_7 - SFPT_1)
#define SFPT_HALF_STEP (SFPT_6_STEPS / 6)
#define SFPT_2 SFPT_MIN + (SFPT_6_STEPS * 1 / 6)
#define SFPT_3 SFPT_MIN + (SFPT_6_STEPS * 2 / 6)
#define SFPT_4 SFPT_MIN + (SFPT_6_STEPS * 3 / 6)
#define SFPT_5 SFPT_MIN + (SFPT_6_STEPS * 4 / 6)
#define SFPT_6 SFPT_MIN + (SFPT_6_STEPS * 5 / 6)


midi_note softpot_get_note(unsigned int position, midi_partial * partial);
void softpot_resolve_position(unsigned int position, unsigned int * under, unsigned int * over, 
        char * d_under, char * d_over);


#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    // TODO If C++ is being used, regular C code needs function names to have C 
    // linkage so the functions can be used by the c code. 

#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* SOFTPOT_H */

