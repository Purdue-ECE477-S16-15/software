/* 
 * File:   adc.h
 * Author: James Alliger <jalliger@purdue.edu>
 *
 * Created on February 9, 2016, 10:06 AM
 */

#ifndef ADC_H
#define	ADC_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>

#define ADC_CHANNEL_5 5
//#define POT_BUF ADC1BUF1
#define SOFTPOT_BUF ADC1BUF9
#define PRESSURESENS_BUF ADC1BUF1
bool ADC_SetConfiguration ();
bool ADC_ChannelEnable ();

// void __attribute__ ((__interrupt__, auto_psv)) _ADC1Interrupt(void)


#ifdef	__cplusplus
}
#endif

#endif	/* ADC_H */

