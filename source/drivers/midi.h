/* 
 * File:   midi.h
 * Author: jalliger
 *
 * Created on February 8, 2016, 12:38 PM
 */

#ifndef MIDI_H
#define	MIDI_H

/*define all midi notes as #defines*/
#define MIDI_C0 0
#define MIDI_CS0 1
#define MIDI_D0 2
#define MIDI_DS0 3
#define MIDI_E0 4
#define MIDI_F0 5
#define MIDI_FS0 6
#define MIDI_G0 7
#define MIDI_GS0 8
#define MIDI_A0 9
#define MIDI_AS0 10
#define MIDI_B0 11
#define MIDI_C1 12
#define MIDI_CS1 13
#define MIDI_D1 14
#define MIDI_DS1 15
#define MIDI_E1 16
#define MIDI_F1 17
#define MIDI_FS1 18
#define MIDI_G1 19
#define MIDI_GS1 20
#define MIDI_A1 21
#define MIDI_AS1 22
#define MIDI_B1 23
#define MIDI_C2 24
#define MIDI_CS2 25
#define MIDI_D2 26
#define MIDI_DS2 27
#define MIDI_E2 28  // partial
#define MIDI_F2 29
#define MIDI_FS2 30
#define MIDI_G2 31
#define MIDI_GS2 32
#define MIDI_A2 33
#define MIDI_AS2 34
#define MIDI_B2 35 // partial
#define MIDI_C3 36
#define MIDI_CS3 37
#define MIDI_D3 38
#define MIDI_DS3 39
#define MIDI_E3 40 // partial
#define MIDI_F3 41
#define MIDI_FS3 42
#define MIDI_G3 43
#define MIDI_GS3 44 // partial
#define MIDI_A3 45
#define MIDI_AS3 46
#define MIDI_B3 47 // partial
#define MIDI_C4 48
#define MIDI_CS4 49
#define MIDI_D4 50 // partial
#define MIDI_DS4 51
#define MIDI_E4 52 // partial
#define MIDI_F4 53
#define MIDI_FS4 54 // partial
#define MIDI_G4 55
#define MIDI_GS4 56
#define MIDI_A4 57
#define MIDI_AS4 58
#define MIDI_B4 59
#define MIDI_C5 60 // max
#define MIDI_CS5 61
#define MIDI_D5 62 // max
#define MIDI_DS5 63
#define MIDI_E5 64
#define MIDI_F5 65
#define MIDI_FS5 66
#define MIDI_G5 67
#define MIDI_GS5 68
#define MIDI_A5 69
#define MIDI_AS5 70
#define MIDI_B5 71
#define MIDI_C6 72
#define MIDI_CS6 73
#define MIDI_D6 74
#define MIDI_DS6 75
#define MIDI_E6 76
#define MIDI_F6 77
#define MIDI_FS6 78
#define MIDI_G6 79
#define MIDI_GS6 80
#define MIDI_A6 81
#define MIDI_AS6 82
#define MIDI_B6 83
#define MIDI_C7 84
#define MIDI_CS7 85
#define MIDI_D7 86
#define MIDI_DS7 87
#define MIDI_E7 88
#define MIDI_F7 89
#define MIDI_FS7 90
#define MIDI_G7 91
#define MIDI_GS7 92
#define MIDI_A7 93
#define MIDI_AS7 94
#define MIDI_B7 95
#define MIDI_C8 96
#define MIDI_CS8 97
#define MIDI_D8 98
#define MIDI_DS8 99
#define MIDI_E8 100
#define MIDI_F8 101
#define MIDI_FS8 102
#define MIDI_G8 103
#define MIDI_GS8 104
#define MIDI_A8 105
#define MIDI_AS8 106
#define MIDI_B8 107
#define MIDI_C9 108
#define MIDI_CS9 109
#define MIDI_D9 110
#define MIDI_DS9 111
#define MIDI_E9 112
#define MIDI_F9 113
#define MIDI_FS9 114
#define MIDI_G9 115
#define MIDI_GS9 116
#define MIDI_A9 117
#define MIDI_AS9 118
#define MIDI_B9 119
#define MIDI_C10 120
#define MIDI_CS10 121
#define MIDI_D10 122
#define MIDI_DS10 123
#define MIDI_E10 124
#define MIDI_F10 125
#define MIDI_FS10 126
#define MIDI_G10 127




#define MIDI_NONOTE 0x7F

#define MIDI_CHAN00 0xF0
#define MIDI_CHAN01 0xF1
#define MIDI_CHAN02 0xF2
#define MIDI_CHAN03 0xF3
#define MIDI_CHAN04 0xF4
#define MIDI_CHAN05 0xF5
#define MIDI_CHAN06 0xF6
#define MIDI_CHAN07 0xF7
#define MIDI_CHAN08 0xF8
#define MIDI_CHAN09 0xF9
#define MIDI_CHAN10 0xFA
#define MIDI_CHAN11 0xFB
#define MIDI_CHAN12 0xFC
#define MIDI_CHAN13 0xFD
#define MIDI_CHAN14 0xFE
#define MIDI_CHAN15 0xFF

// Program (Instrument) Change
#define MIDI_PROGRAM_CHANGE_STATUS 0b11001111
#define MIDI_PATCH_PIANO 3
#define MIDI_PATCH_GUITAR 27
#define MIDI_PATCH_TROMBONE 57

//pitch bend
#define MIDI_P_BEND_CMD 0b11101111
#define MIDI_P_BEND_CENTER 64
#define MIDI_P_BEND_HALF 32

//reset commands
#define MIDI_CHANNEL_MODE 0xBF
#define MIDI_ALL_SOUND_OFF 120
#define MIDI_RESET_ALL_CONTROLLERS 121
#define MIDI_ALL_NOTES_OFF 123

//control change
#define MIDI_CONTROL_CHANGE 0b10111111
#define MIDI_ATTACK_TIME 0b01001001
#define MIDI_RELEASE_TIME 0b01001000
#define MIDI_CHANNEL_VOLUME 0b00000111

#define MIDI_PORTAMENTO_ON_CMD 65
#define MIDI_PORTAMENTO_ON 120
#define MIDI_PORTAMENTO_OFF 1
#define MIDI_PORTAMENTO_TIME 5
#define MIDI_PORTAMENTO_CONTROL 84

typedef struct midi_note{
  char note;
  char bend;
  char velocity;
} midi_note;

typedef struct midi_partial{
    char positions[7];
} midi_partial;

int midi_message_transmit(char status, char data0,  char data1);
int midi_noteon(char channel, char note, char velocity);
int midi_noteoff(char channel, char note, char velocity);

int midi_program_change(char channel, char patch);

void midi_reset(char channel);
void midi_init_controller(char channel);

char midi_start_slide(char channel, midi_note note);
char midi_change_slide(char channel, char old_note, midi_note new_note);
void midi_init_partial(midi_partial * p, char num);

#ifdef	__cplusplus
extern "C" {
#endif



#ifdef	__cplusplus
}
#endif

#endif	/* MIDI_H */


