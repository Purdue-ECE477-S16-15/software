/******************************************************************************/
/* Files to Include                                                           */
/******************************************************************************/

/* Device header file */
//#if defined(__XC16__)
//    #include <xc.h>
//#elif defined(__C30__)
//    #if defined(__PIC24E__)
//    	#include <p24Exxxx.h>
//    #elif defined (__PIC24F__)||defined (__PIC24FK__)
//	#include <p24Fxxxx.h>
//    #elif defined(__PIC24H__)
//	#include <p24Hxxxx.h>
//    #endif
//#endif

#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>
#include <xc.h>
//#include <p24FJ256GA110.h>       /* Includes true/false definition                  */

#include "../system/system.h"        /* System funct/params, like osc/peripheral config */
#include "../app/app.h"
#include "uart.h"
#include "midi.h"          /* User funct/params, such as InitApp              */

/*
 * int midi_message_transmit(char status,  char data0, char data1)
 * general midi transmit message. abstract send three byte message. 
 * sends status and two bytes. Not designed to be used frequently
 */
int inline midi_message_transmit(char status,  char data0, char data1)
{
#ifdef UART_BUFFER
    WriteTxBuffer(status);
    WriteTxBuffer(data0);
    WriteTxBuffer(data1);
    UART_TxStart();
    return 1;
#else
    U2TXREG = status;
    while(U2STAbits.UTXBF != 0);
    U2TXREG = data0;
    while(U2STAbits.UTXBF != 0);
    U2TXREG = data1;
    while(U2STAbits.UTXBF != 0);
    return 1;
#endif
}

int inline midi_message_transmit_onebyte(char status, char data)
{
    WriteTxBuffer(status);
    WriteTxBuffer(data);
    UART_TxStart();
    return 1;
}

int midi_program_change(char channel, char patch)
{
    return midi_message_transmit_onebyte(channel & MIDI_PROGRAM_CHANGE_STATUS, patch & 0x7F);
}

int midi_noteon(char chan, char note, char velocity)
{
    return midi_message_transmit(chan & 0x9F, note & 0x7F, velocity &0x7F);
}

int midi_noteoff(char chan, char note, char velocity)
{
    return midi_message_transmit(chan & 0x8F, note & 0x7F, velocity &0x7F);
}

int midi_cross_fade(char channel, char note1, char note2, char velocity, unsigned int ms)
{
    return -1;
}

void midi_reset(char channel)
{
    midi_message_transmit(MIDI_CHANNEL_MODE & channel, MIDI_ALL_SOUND_OFF, 0);
    midi_message_transmit(MIDI_CHANNEL_MODE & channel, MIDI_RESET_ALL_CONTROLLERS, 0);
    midi_message_transmit(MIDI_CHANNEL_MODE & channel, MIDI_ALL_NOTES_OFF, 0);
    
}

void midi_init_controller(char channel)
{
    midi_reset(channel);
    
    midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_ATTACK_TIME, 0);
    midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_RELEASE_TIME, 0);
    midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_PORTAMENTO_ON_CMD, MIDI_PORTAMENTO_OFF);
    midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_PORTAMENTO_TIME, 0);
    midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, MIDI_P_BEND_CENTER);
}

//turn on note with pitch bend
//bend should be at most a half step from center of note
//returns the pitch being played or -1
char midi_start_slide(char channel, midi_note note)
{
    if(note.bend > MIDI_P_BEND_CENTER + MIDI_P_BEND_HALF || 
       note.bend < MIDI_P_BEND_CENTER - MIDI_P_BEND_HALF)
        return -1;
    
    midi_noteon(channel, note.note, note.velocity);
    midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, note.bend);
    return note.note;
}

//continue old_note into new_note
//bend should be at most a half step from center of note
//returns the new pitch being played or -1
char midi_change_slide(char channel, char old_note, midi_note new_note)
{
    if(new_note.bend > MIDI_P_BEND_CENTER + MIDI_P_BEND_HALF || 
       new_note.bend < MIDI_P_BEND_CENTER - MIDI_P_BEND_HALF)
        return -1;
    
    if(new_note.note == old_note)
    {//just change pitch bend
        midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, new_note.bend);
    }
    else
    {//portamento from old note to new note
        midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_PORTAMENTO_CONTROL, old_note);
        midi_noteon(channel, new_note.note, new_note.velocity);
        midi_noteoff(channel, old_note, 127);
        midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, new_note.bend);
    }
    return new_note.note;
}

void midi_init_partial(midi_partial * p, char num)
{
    switch(num)
    {
        case(0):
            p->positions[6] = MIDI_E2;
            break;
        case(1):
            p->positions[6] = MIDI_B2;
            break;
        case(2):
            p->positions[6] = MIDI_E3;
            break;       
        case(3):
            p->positions[6] = MIDI_GS3;
            break;
        case(4):
            p->positions[6] = MIDI_B3;
            break;
        case(5):
            p->positions[6] = MIDI_D4;
            break;
        case(6):
            p->positions[6] = MIDI_E4;
            break;
        case(7):
            p->positions[6] = MIDI_FS4;
            break;
        default:
            p->positions[6] = MIDI_E2;
            break;
    }
    p->positions[5] = p->positions[6] + 1;
    p->positions[4] = p->positions[6] + 2;
    p->positions[3] = p->positions[6] + 3;
    p->positions[2] = p->positions[6] + 4;
    p->positions[1] = p->positions[6] + 5;
    p->positions[0] = p->positions[6] + 6;  
}