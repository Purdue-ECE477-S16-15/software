/* 
 * SPI Transmit-Only Driver Source
 */

#include <xc.h>
#include <stdint.h>

#include "spi.h"

void spi_init()
{
    SPI1CON1bits.MODE32 = 0;    // 8-bit Mode
    SPI1CON1bits.MODE16 = 0;
    
    SPI1CON1bits.MSTEN = 1;     // Master Mode
    SPI1CON1bits.CKE = 1;       // Transmit on falling edge
    
    SPI1STATLbits.SPIROV = 0;   // Receive Overflow Status Bit = No Overflow
    SPI1CON1bits.SPIEN = 1;     // Enable SPI
}

void spi_write(uint8_t data)
{
    SPI1BUFL = data;
}