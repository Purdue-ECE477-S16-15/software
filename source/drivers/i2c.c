#include <xc.h>
#include "i2c.h"
#include <stdbool.h> 
#include <stdint.h>

uint16_t jDone; //boolian flag to say that interupt done

I2CEMEM_DRV i2cMem;
void I2C_init(){
    i2cMem.cmd = 0;
    i2cMem.oData = 0;
    
    I2C2BRG = 0x27; // set the I2C to comunicate at 100KHz for right now
    I2CENABLE = 1; //enable I2C   
    IEC3bits.MI2C2IE = 0;//TODO: move t0 i2c 2
    IFS3bits.MI2C2IF = 1;
}

 inline void I2C_start(){
    IEC3bits.MI2C2IE = 1; //enable interrupts and start the state machine
 }

 inline void I2C_send(char data){
     I2C2TRN = data;
 }
 
 void __attribute__ ((__interrupt__, auto_psv)) _MI2C2Interrupt(void){
    static int16_t state = 0;
    static int16_t  cntr = 0;
    static int16_t  rtrycntr = 0;
    
    switch (state){
        case 0: //done with idle, start new message
            IFS3bits.MI2C2IF = 0;
            I2CSTART = 1; //send start command
            state = state +1;
            break;
        case 1: //start Byte with device address and read or write 
            IFS3bits.MI2C2IF = 0;
            if(i2cMem.cmd == I2C_WRITE || i2cMem.cmd == I2C_READ){
                I2C2TRN = i2cMem.oData ->csel << 1;//inherent write bit in position 0
            }
            state = state + 1;
            break;
        case 2: //send register address if a device acked
            IFS3bits.MI2C2IF = 0;
            if(I2C2STATbits.ACKSTAT == 1) //ack not received, will retry
            {
                if( rtrycntr < MAX_RETRY)
                {
                    I2CPEN = 1;
                    rtrycntr++;
                    state = 0; //lets try this again after stopping
                }
                else{
                   I2CPEN = 1;
                   state = 11;
                }
            }
            else{
                rtrycntr = 0;
                //only works with one byte because thats all that is needed
                I2C2TRN = i2cMem.oData -> addr;
                state = state + 1;
            }
            break;
        case 3: //data phase
            IFS3bits.MI2C2IF = 0;
            if( I2C2STATbits.ACKSTAT == 1)
            {
                I2CPEN = 1;
                state = 11;
            }
            else
            {
                if(i2cMem.cmd == I2C_WRITE)//write data phase
                {
                    I2C2TRN = *(i2cMem.oData ->buff +cntr);
                    state = state + 1;
                    cntr = cntr +1;
                }
                else if(i2cMem.cmd == I2C_READ) //start read data phase
                {
                    I2CRSEN = 1; //repeat start
                    state = 5; //goto state to resend address
                }
            }
            break;
        case 4: //writing more data, or not
            IFS3bits.MI2C2IF = 0;
            if(I2C2STATbits.ACKSTAT == 1)
            {
                I2CPEN = 1;
                state = 11;
            }
            else
            {
                if( cntr ==i2cMem.oData -> n)
                {
                    I2CPEN = 1;
                    state = 10; 
                }
                else
                {
                    I2C2TRN = *(i2cMem.oData ->buff +cntr);
                    cntr = cntr + 1;
                    //state should remain the same
                }
            }
            break;
        case 5: //resend device address with read mode now
            IFS3bits.MI2C2IF = 0;
            I2C2TRN = (i2cMem.oData ->csel << 1)| 0x1; //set read flag
            state = state + 1;
            break;
        case 6: //set master to read mode following address
            IFS3bits.MI2C2IF = 0;
            if(I2C2STATbits.ACKSTAT == 1)
            {
                I2CPEN = 1;
                state = 11;
            }
            else
            {
                I2CRCEN = 1;
                state = state + 1;
            }
            break;
        case 7:
            IFS3bits.MI2C2IF = 0;
            *(i2cMem.oData -> buff + cntr) = I2C2RCV;
            cntr++;
            if( cntr == i2cMem.oData->n)
            {
                I2CACKDT = 1; //no ack
                state = state + 2; //go to stop 
            }
            else
            {
                I2CACKDT = 0; //ack
                state = state + 1; // ask for another byte
                
            }
            I2CACKEN = 1; //enable acking

            break;
        case 8: //read another address, different from 6 because no ack check
            IFS3bits.MI2C2IF = 0;
            I2CRCEN = 1;
            state = state - 1;
            break;
        case 9: //stop case
            IFS3bits.MI2C2IF = 0;
            I2CPEN = 1;
            state = state + 1;
            break;
        case 10: //stop over, clear all states and start over
            state = 0;
            cntr = 0;
            i2cMem.cmd = I2C_IDLE;
            IEC3bits.MI2C2IE = 0;// disable but do not clear the interrupt
            break;
        case 11: //error trap with no return
            state = 0;
            rtrycntr = 0;
            cntr = 0;
            i2cMem.cmd = I2C_ERR;
            IEC3bits.MI2C2IE = 0;
            break;
        
            
    }
 }
