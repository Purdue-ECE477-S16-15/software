/*
 * File:   lcd.c
 * Author: lneuman
 *
 * Created on April 7, 2016, 9:40 AM
 */


#include <xc.h>
#include "lcd.h"
#include "spi.h"

#define FOSC    (7370000ULL)
#define FCY     (FOSC/2)
#include <libpic30.h>

void lcd_init()
{
//    TRISBbits.TRISB10 = 0;
//    TRISBbits.TRISB11 = 0;
//    TRISBbits.TRISB12 = 0;
//    ANSBbits.ANSB12 = 0;
    __delay_ms(1000);

    LCD_RS = 1;
    LCD_CLK = 1;
    lcd_send_i(LCD_ON);
    lcd_send_i(LCD_TWOLINE);
    lcd_send_i(LCD_CLR);
    __delay_ms(1000);

}

void lcd_shiftout(char x)
{
    spi_write(x);
    while(!SPI1STATLbits.SRMT){};
    __delay_us(20);
}

void lcd_send_byte(uint8_t x)
{
    lcd_shiftout(x);
    LCD_CLK = 0;
    LCD_CLK = 1;
    LCD_CLK = 0;
    __delay_us(850);
}

void lcd_send_i(char x)
{
    LCD_RS = 0;
    lcd_send_byte(x);
}

void lcd_chgline(char x)
{
    lcd_send_i(LCD_CURMOV);
    lcd_send_i(x); 
}

void lcd_print_c(char x)
{
    LCD_RS = 1;
    lcd_send_byte(x);
}

void lcd_pmsg(char* x)
{
    int i;
    for(i=0; x[i] != '\0'; i++)
    {
        lcd_print_c(x[i]);
    }
}