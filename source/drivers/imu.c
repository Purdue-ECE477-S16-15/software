#include <xc.h>
#include <float.h>
#include <math.h>
#include <libq.h>
#include "imu.h"
#include "i2c.h"


uint16_t        AccelBuf[10], MagBuff[10];
I2CEMEM_DATA    Mag;
I2CEMEM_DATA    Accel;

float pitch;
float heading;

int16_t temp;

Cartesian_t Acc_pos, Mag_pos;
extern I2CEMEM_DRV i2cMem;
void IMU_init(){
    // nothing right now
    AccelBuf[0] = 0x57;
    Accel.buff = AccelBuf;
    Accel.n = 1;
    Accel.addr = ACC_CTRL1;
    Accel.csel = LSM303_ACC;
    
    i2cMem.oData = &Accel;
    i2cMem.cmd = I2C_WRITE;
    I2C_start();
    while(i2cMem.cmd != I2C_IDLE){}
    
    Mag.buff = MagBuff;
    MagBuff[0] = 0xD8;
    Mag.n = 1;
    Mag.addr = MAG_CTRL_REG1;
    Mag.csel = LSM303_MAG;
    
    i2cMem.oData = &Mag;
    i2cMem.cmd = I2C_WRITE;
    
    I2C_start();
    while(i2cMem.cmd != I2C_IDLE){}
    
    MagBuff[0] = 0x00;
    Mag.n = 1;
    Mag.addr = MAG_CTRL_REG3;
    i2cMem.cmd = I2C_WRITE;
    
    I2C_start();
    while(i2cMem.cmd != I2C_IDLE){}
    
    MagBuff[0] = 0x04;
    Mag.n = 1;
    Mag.addr = MAG_CTRL_REG4;
    i2cMem.cmd = I2C_WRITE;
    
    I2C_start();
    while(i2cMem.cmd != I2C_IDLE){}
   
}

void IMU_progress(){
    static IMU_state state = idle;
    
    switch(state){
        case idle:
            state = start_acc_read;
        case start_acc_read:
            //try and read from accelerometer
            Accel.csel = LSM303_ACC;
            Accel.n = 6;
            Accel.buff = AccelBuf;
            Accel.addr = ACC_OUT_X_L | IMU_INC_MASK;
            i2cMem.oData = &Accel;
            i2cMem.cmd = I2C_READ;
            I2C_start();
            state = calc_heading;
            break;
        case read_acc:
            if(i2cMem.cmd == I2C_IDLE){
                Acc_pos.x = AccelBuf[0] | (AccelBuf[1] <<8);
                Acc_pos.y = AccelBuf[2] | (AccelBuf[3] <<8);
                Acc_pos.z = AccelBuf[4] | (AccelBuf[5] <<8);

                state = start_mag_read;
            }
            else if(i2cMem.cmd == I2C_ERR){
                //try try again
                i2cMem.cmd = I2C_READ;
                I2C_start();
            }
            break;
        case start_mag_read:
            Mag.csel = LSM303_MAG;
            Mag.n = 8;
            Mag.buff = MagBuff;
            Mag.addr = MAG_OUTX_L | IMU_INC_MASK;
            i2cMem.oData = &Mag;
            i2cMem.cmd = I2C_READ;
            I2C_start();
            state = calc_pitch;
            break;
        case read_mag:
            if(i2cMem.cmd == I2C_IDLE){
                Mag_pos.x = MagBuff[0] | (MagBuff[1] <<8);
                Mag_pos.y = MagBuff[2] | (MagBuff[3] <<8);
                Mag_pos.z = MagBuff[4] | (MagBuff[5] <<8);
                temp = MagBuff[6] | (MagBuff[7] <<8);
                state = start_acc_read;
            }
            else if(i2cMem.cmd == I2C_ERR){
                //try try again
                i2cMem.cmd = I2C_READ;
                I2C_start();
            }
            break;
        case calc_heading:
            state = read_acc;
            break;
        case calc_pitch:
            pitch =((float)Acc_pos.y / (float)MAX_GRAV_Y);
            pitch = asin(pitch);
            state = read_mag;
            break;
        default:
            state = idle;
    }
}