// Button Functions, i.e. ":-P James"

#ifndef BUTTON_H
#define	BUTTON_H

#include <xc.h> // include processor files - each processor file is guarded.  

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */


//#define SW0 PORTCbits.RC0
//#define SW1 PORTCbits.RC1
//#define SW2 PORTCbits.RC2
//#define SW3 PORTCbits.RC3
//#define SW4 PORTCbits.RC4
//#define SW5 PORTCbits.RC5
//#define SW6 PORTCbits.RC6
//#define SW7 PORTCbits.RC7

#define BUTTON_BITMAP PORTC
    
typedef enum {
    BUTTON_LEFT     = 3, // Switch 0 / 0 bit position in bitmap
    BUTTON_RIGHT    = 6,
    BUTTON_DOWN     = 4,
    BUTTON_A        = 7,
    BUTTON_B        = 5,
    BUTTON_C        = 2,
    BUTTON_D        = 1
} button_t;

#define NUM_BUTTONS 7

void sample_buttons(void);
int get_button(button_t button);
int get_button_rising_edge(button_t button);
int get_button_falling_edge(button_t button);

int get_button_flag(button_t button);


#ifdef	__cplusplus
}
#endif /* __cplusplus */

#endif	/* BUTTON_H */
    
