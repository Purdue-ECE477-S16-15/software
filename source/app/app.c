#include "app.h"
#include "../drivers/adc.h"
#include "../drivers/softpot.h"
#include "button.h"
#include "menu.h"

static NOTES_STATE note_state;
static unsigned int presSense0_calibration;

midi_partial p0, p1, p2, p3, p4, p5, p6, p7;
midi_note mnote;
midi_note mchord;
char chord = 7;
char chord_on = 0;
char pnum;
char old_pnum;
midi_partial p;
char old_note;

char channel;

// Global menu objects
menu_setting_t ch1_setting, ch2_setting, instr_setting, tilt_setting, key_setting, playback_setting;
menu_t channel_menu, effects_menu, config_menu, main_menu;

void InitApp()
{
    channel = MIDI_CHAN00;
    midi_program_change(channel, MIDI_PATCH_TROMBONE);
    
    presSense0_calibration = 0x204;
    note_state.pitch_bend = 64;//no pitchbend
    midi_init_partial(&p0,0);
    midi_init_partial(&p1,1);
    midi_init_partial(&p2,2);
    midi_init_partial(&p3,3);
    midi_init_partial(&p4,4);
    midi_init_partial(&p5,5);
    midi_init_partial(&p6,6);
    midi_init_partial(&p7,7);
    p = p0;
    pnum = 0;
    old_pnum = pnum;
}

void update_midi_state()
{
    static midi_state state = note_off;
    
    unsigned int new_pressure = PRESSURESENS_BUF ;
    unsigned int new_softpot = SOFTPOT_BUF ;
    
    //read button status to set partial
    unsigned int abc = !get_button(BUTTON_A) +
                        (!get_button(BUTTON_B) << 1) +
                        (!get_button(BUTTON_C) << 2);
   old_pnum = pnum; 
   switch(abc)
    {
        case 0:
	    pnum = 0;
            p = p0;
            break;
        case 1:
	    pnum = 1;
            p = p1;
            break;
        case 2:
	    pnum = 2;
            p = p2;
            break;
        case 3:
	    pnum = 3;
            p = p3;
            break;
        case 4:
	    pnum = 4;
            p = p4;
            break;
        case 5:
	    pnum = 5;
            p = p5;
            break;
        case 6:
	    pnum = 6;
            p = p6;
            break;
        case 7:
	    pnum = 7;
            p = p7;
            break;
        default:
	    pnum = 4;
            p = p4;
    }
    
    switch(state){
        case note_off:
            if( new_pressure > (presSense0_calibration + ON_THRESH))
            {
                char volume = new_pressure >> 3;
                state = note_on;//TODO: get pressure to sent
                mnote = softpot_get_note(new_softpot, &p);
                mnote.velocity = volume;
                midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_CHANNEL_VOLUME,
                        volume);
                midi_start_slide(channel, mnote);
                if(!get_button(BUTTON_D))
                {
                    mchord = mnote;
                    mchord.note += chord;
                    midi_start_slide(channel, mchord);
                    chord_on = 1;
                }
            }
            break;
        case note_on:
            
            if( new_pressure < (presSense0_calibration + OFF_THRESH))
            {
                state = note_off;
                midi_noteoff(channel, mnote.note, 127);
                midi_noteoff(channel, mchord.note, 127);
                midi_message_transmit(MIDI_CHANNEL_MODE & channel, MIDI_ALL_NOTES_OFF, 0);
                chord_on = 0;
            }
            else{
                char volume = new_pressure >> 3;
                old_note = mnote.note;
                char old_velocity = mnote.velocity;
                mnote = softpot_get_note(new_softpot, &p);
                mnote.velocity = old_velocity;
                midi_message_transmit(MIDI_CONTROL_CHANGE & channel, MIDI_CHANNEL_VOLUME,
                        volume);
                if(pnum == old_pnum)
                    midi_change_slide(channel, old_note, mnote);
                else
                {
                    midi_noteoff(channel, old_note, 127);
                    midi_noteon(channel, mnote.note, mnote.velocity);
                    midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, mnote.bend);
                }
                
                if(!chord_on)
                {
                   if(!get_button(BUTTON_D))
                    {//start a chord
                        mchord = mnote;
                        mchord.note += chord;
                        midi_start_slide(channel, mchord);
                        chord_on = 1;
                    } 
                }
                else
                {//chord is on
                    if(!get_button(BUTTON_D))
                    {//continue a chord
                        char old_chord = mchord.note;
                        mchord = mnote;
                        mchord.note += chord;
                        if(pnum == old_pnum)
                            midi_change_slide(channel, old_chord, mchord);
                    	else
                        {
                            midi_noteoff(channel, old_chord, 127);
                            midi_noteon(channel, mchord.note, mchord.velocity);
                            midi_message_transmit(MIDI_P_BEND_CMD & channel, 0, mchord.bend);
                        }
		    }
                    else if(get_button(BUTTON_D))
                    {//stop a chord
                        midi_noteoff(channel, mchord.note, 127);
                        chord_on = 0;
                    }
                }
            }
            break;
        default:
            state = note_off;
            break;                        
    }
    
}

void disp_option(int option)
{
    
}

void change_channel(int option)
{
    midi_reset(channel);
    
    switch (option)
    {
        case 0:     channel = MIDI_CHAN00; break;
        case 1:     channel = MIDI_CHAN01; break;
        case 2:     channel = MIDI_CHAN02; break;
        case 3:     channel = MIDI_CHAN03; break;
        case 4:     channel = MIDI_CHAN04; break;
        case 5:     channel = MIDI_CHAN05; break;
        case 6:     channel = MIDI_CHAN06; break;
        case 7:     channel = MIDI_CHAN07; break;
        case 8:     channel = MIDI_CHAN08; break;
        case 9:     channel = MIDI_CHAN09; break;
        case 10:    channel = MIDI_CHAN10; break;
        case 11:    channel = MIDI_CHAN11; break;
        case 12:    channel = MIDI_CHAN12; break;
        case 13:    channel = MIDI_CHAN13; break;
        case 14:    channel = MIDI_CHAN14; break;
        case 15:    channel = MIDI_CHAN15; break;
    }
}

void change_instrument(int option)
{
    switch (option)
    {
        case 0:
            midi_program_change(channel, MIDI_PATCH_TROMBONE);
            break;
        case 1:
            midi_program_change(channel, MIDI_PATCH_GUITAR);            
            break;
        case 2:
            midi_program_change(channel, MIDI_PATCH_PIANO);
            break;
    }
}

void change_chord(int option)
{
    if(option == 0)
    {//set to power chord
        chord = 7;
    }
    else if(option == 1)
    {//set to major 3rd
        chord = 4;
    }
}

void menu_init(){
    ch1_setting = menu_create_setting("Primary Channel", change_channel);
    menu_setting_add_option(&ch1_setting, "1");
    menu_setting_add_option(&ch1_setting, "2");
    menu_setting_add_option(&ch1_setting, "3");
    menu_setting_add_option(&ch1_setting, "4");
    menu_setting_add_option(&ch1_setting, "5");
    menu_setting_add_option(&ch1_setting, "6");
    menu_setting_add_option(&ch1_setting, "7");
    menu_setting_add_option(&ch1_setting, "8");
    menu_setting_add_option(&ch1_setting, "9");
    menu_setting_add_option(&ch1_setting, "10");
    menu_setting_add_option(&ch1_setting, "11");
    menu_setting_add_option(&ch1_setting, "12");
    menu_setting_add_option(&ch1_setting, "13");
    menu_setting_add_option(&ch1_setting, "14");
    menu_setting_add_option(&ch1_setting, "15");
    menu_setting_add_option(&ch1_setting, "16");
    
    ch2_setting = menu_create_setting("Secondary Ch.", disp_option);
    menu_setting_add_option(&ch2_setting, "1");
    menu_setting_add_option(&ch2_setting, "2");
    menu_setting_add_option(&ch2_setting, "3");
    menu_setting_add_option(&ch2_setting, "4");
    menu_setting_add_option(&ch2_setting, "5");
    menu_setting_add_option(&ch2_setting, "6");
    menu_setting_add_option(&ch2_setting, "7");
    menu_setting_add_option(&ch2_setting, "8");
    menu_setting_add_option(&ch2_setting, "9");
    menu_setting_add_option(&ch2_setting, "10");
    menu_setting_add_option(&ch2_setting, "11");
    menu_setting_add_option(&ch2_setting, "12");
    menu_setting_add_option(&ch2_setting, "13");
    menu_setting_add_option(&ch2_setting, "14");
    menu_setting_add_option(&ch2_setting, "15");
    menu_setting_add_option(&ch2_setting, "16");
    
    channel_menu = menu_create();
    menu_add_child_setting(&channel_menu, "Primary Ch.", &ch1_setting);
    menu_add_child_setting(&channel_menu, "Secondary Ch.", &ch2_setting);
    

    instr_setting = menu_create_setting("Instrument", change_instrument);
    menu_setting_add_option(&instr_setting, "Trombone");
    menu_setting_add_option(&instr_setting, "Guitar");
    menu_setting_add_option(&instr_setting, "Piano");
    
    
    tilt_setting = menu_create_setting("Tilt Effect", disp_option);
    menu_setting_add_option(&tilt_setting, "Channel Phase");
    menu_setting_add_option(&tilt_setting, "Instr. Change");
    menu_setting_add_option(&tilt_setting, "Volume Control");

    key_setting = menu_create_setting("D Key Effect", change_chord);
    menu_setting_add_option(&key_setting, "Power Chord");
    menu_setting_add_option(&key_setting, "Major 3rd");
    
    effects_menu = menu_create();
    menu_add_child_setting(&effects_menu, "Tilt Effect", &tilt_setting);
    menu_add_child_setting(&effects_menu, "Key Effect", &key_setting);
    
    config_menu = menu_create();
    menu_add_child_setting(&config_menu, "MIDI Channel", &ch1_setting);
    menu_add_child_menu(&config_menu, "Effects", &effects_menu);
    menu_add_child_setting(&config_menu, "Instrument", &instr_setting);
    
    playback_setting = menu_create_setting("Enable Playback", disp_option);
    menu_setting_add_option(&playback_setting, "Disable");
    menu_setting_add_option(&playback_setting, "Enable");
    
    main_menu = menu_create();
    menu_add_child_menu(&main_menu, "Configuration", &config_menu);
    menu_add_child_setting(&main_menu, "Playback", &playback_setting);
    
    
    /*
     * Use Menu
     */
    menu_set_main(&main_menu);
    menu_update_display();
}

void menu_progress(){
    if(get_button_flag(BUTTON_DOWN))
    {
        menu_next();
    }
    else if(get_button_flag(BUTTON_LEFT))
    {
        menu_cancel();
    }
    else if(get_button_flag(BUTTON_RIGHT))
    {
        menu_enter();
    }
}
