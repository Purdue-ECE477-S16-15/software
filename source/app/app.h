/* 
 * File:   app.h
 * Author: Subhav Ramachandran <ramachas@purdue.edu>
 *
 * Created on February 24, 2016, 10:32 AM
 */

#ifndef APP_H
#define	APP_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdbool.h>
/**
 * Application Configuration Data
 * 
 * @TODO Update this, expect this to change during prototyping.
 */
    
#define ON_THRESH  0x10
#define OFF_THRESH 0x0A
typedef struct
{
    //midi information 
    unsigned char velocity; 
    unsigned char note;
    unsigned char old_velocity; 
    unsigned char old_note;
    unsigned char pitch_bend;
    
    bool note_on;
    
    bool note_to_send;
    
    //state transition from calibrated to uncalibrated mode
    bool calibrated;
} NOTES_STATE;

typedef enum{
    note_off,
    note_on        
            
}midi_state;


void InitApp(void);         /* I/O and Peripheral Initialization */

/**
 * Update the note being output if necessary.
 * 
 * This is meant to be called after an interrupt.
 * 
 * @TODO Think of params here.
 */
void update_midi_state(void);


void menu_init();
void menu_progress();


#ifdef	__cplusplus
}
#endif

#endif	/* APP_H */

