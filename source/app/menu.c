#include <stdlib.h>
#include <stdio.h>

#include "menu.h"
#include "../drivers/lcd.h"

// If curr_setting is NULL, display the curr_menu
static menu_t* curr_menu = NULL;
static menu_setting_t* curr_setting = NULL;


/* Menu object functions */

menu_t menu_create()
{
    menu_t ret;
    
    ret.parent = NULL;
    ret.num_items = 0;
    ret.curr_item = 0;
    
    return ret;
}

menu_setting_t menu_create_setting(char* title, void(*callback)(int))
{
    menu_setting_t ret;
    
    ret.title = title;
    ret.callback = callback;
    
    ret.num_options = 0;
    ret.curr_option = 0;
    
    return ret;
}

void menu_add_child_menu(menu_t* menu, char* name, menu_t* child)
{
    child->parent = menu;
    
    menu->items[menu->num_items].name = name;
    
    menu->items[menu->num_items].menu = child;
    menu->items[menu->num_items].setting = NULL;
    
    (menu->num_items)++;
}

void menu_add_child_setting(menu_t* menu, char* name, menu_setting_t* setting)
{
    menu->items[menu->num_items].name = name;
    
    menu->items[menu->num_items].menu = NULL;
    menu->items[menu->num_items].setting = setting;
    
    (menu->num_items)++;
}


void menu_setting_add_option(menu_setting_t* setting, char* option)
{
    setting->options[setting->num_options] = option;
    
    (setting->num_options)++;
}




/* Interface functions */

void menu_set_main(menu_t* root)
{
    curr_menu = root;
}

void menu_update_display()
{
    char buf[17];
    
    lcd_send_i(LCD_CLR);
    
    if(curr_setting == NULL)    // Display a menu screen
    {
        if(curr_menu->num_items == 1 || curr_menu->curr_item < curr_menu->num_items - 1) // Not at bottom of menu
        {
            sprintf(buf, "> %s", curr_menu->items[curr_menu->curr_item].name);
            lcd_chgline(LINE1);
            lcd_pmsg(buf);
            
            if(curr_menu->num_items > 1)
            {
                sprintf(buf, "  %s", curr_menu->items[curr_menu->curr_item + 1].name);
                lcd_chgline(LINE2);
                lcd_pmsg(buf);
            }
        }
        else // Reached bottom of menu
        {
            sprintf(buf, "  %s", curr_menu->items[curr_menu->curr_item - 1].name);
            lcd_chgline(LINE1);
            lcd_pmsg(buf);
            sprintf(buf, "> %s", curr_menu->items[curr_menu->curr_item].name);
            lcd_chgline(LINE2);
            lcd_pmsg(buf);
        }
    }
    
    else    // Display a setting screen
    {
            sprintf(buf, "%s:", curr_setting->title);
            lcd_chgline(LINE1);
            lcd_pmsg(buf);
            
            sprintf(buf, "> %s", curr_setting->options[curr_setting->curr_option]);
            lcd_chgline(LINE2);
            lcd_pmsg(buf);
    }
}

void menu_next()
{
    if(curr_setting == NULL)
    {
        curr_menu->curr_item = (curr_menu->curr_item + 1) % curr_menu->num_items;
    }
    else
    {
        curr_setting->curr_option = (curr_setting->curr_option + 1) % curr_setting->num_options;
    }
    
    menu_update_display();
}

void menu_enter()
{
    if(curr_setting == NULL)
    {
        menu_item_t selected = curr_menu->items[curr_menu->curr_item];
        
        if(selected.setting == NULL)
        {
            curr_menu = selected.menu;
        }
        else
        {
            curr_setting = selected.setting;
        }
    }
    else
    {
        curr_setting->callback(curr_setting->curr_option);
        curr_setting = NULL;
    }
    
    
    menu_update_display();
}

void menu_cancel()
{
    if(curr_setting == NULL)
    {
        if(curr_menu->parent != NULL)
        {
            curr_menu = curr_menu->parent;
        }
    }
    else
    {
        curr_setting = NULL;
    }
    
    
    menu_update_display();
}