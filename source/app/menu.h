#ifndef MENU_H
#define	MENU_H

#include <xc.h> // include processor files - each processor file is guarded.  

#define MENU_MAX_ITEMS 10
#define MENU_MAX_SETTING_OPTIONS 16

/*
 * Structs/Types
 */

typedef struct menu_setting
{
	char* title;

	int num_options;
	char* options[MENU_MAX_SETTING_OPTIONS];
	int curr_option;

	void (* callback)(int curr_option);
} menu_setting_t;



typedef struct menu_item
{
	char* name;

	struct main_menu* menu; // If this is NULL, it's a setting, not a menu?
	menu_setting_t* setting;
} menu_item_t;

typedef struct main_menu
{
	struct main_menu* parent; // If this is NULL, do nothing on cancel

	int num_items;
	menu_item_t items[MENU_MAX_ITEMS];
	int curr_item;
} menu_t;


/*
 * Functions
 */

menu_t menu_create();
menu_setting_t menu_create_setting(char* title, void (* callback)(int curr_option));

void menu_add_child_menu(menu_t* menu, char* name, menu_t* child);
void menu_add_child_setting(menu_t* menu, char* name, menu_setting_t* setting);

void menu_setting_add_option(menu_setting_t* setting, char* option);

void menu_set_main(menu_t* root);
void menu_update_display();

void menu_cancel();
void menu_next();
void menu_enter();


#endif	/* MENU_H */
