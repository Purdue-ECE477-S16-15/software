
#include "button.h"
#include <stdbool.h>

static unsigned int second_last_value;
static unsigned int last_value;
bool down_falling;
bool left_falling;
bool right_falling;


void sample_buttons()
{
    second_last_value = last_value;
    last_value = BUTTON_BITMAP;
    
    if(get_button_falling_edge(BUTTON_DOWN)){
        down_falling = true;
    }
    
    if(get_button_falling_edge(BUTTON_LEFT)){
        left_falling = true;
    }
    
    if(get_button_falling_edge(BUTTON_RIGHT)){
        right_falling = true;
    }
}

static int _get_last_value(button_t button)
{
    return (last_value & (1 << button)) > 0;
}

static int _get_second_last_value(button_t button)
{
    return (second_last_value & (1 << button)) > 0;
}



int get_button(button_t button)
{
    return _get_last_value(button);
}

int get_button_rising_edge(button_t button)
{
    return _get_second_last_value(button) == 0 && _get_last_value(button) == 1;
}

int get_button_falling_edge(button_t button)
{
    return _get_second_last_value(button) == 1 && _get_last_value(button) == 0;
}

int get_button_flag(button_t button){
    bool * button_flag;
    switch(button){
        case BUTTON_DOWN:
            button_flag = &down_falling;
            break;
        case BUTTON_LEFT:
            button_flag = &left_falling;
            break;
        case BUTTON_RIGHT:
            button_flag = &right_falling;
            break;
        default:
            return 0;
    }
    
    if(*button_flag){
        *button_flag = 0;
        return 1;
    }
    
    return 0;
}
